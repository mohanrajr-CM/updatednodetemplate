
### Running the application
> install packages locally
```
npm install
```

### Database dummy data install
> migrate the database
```
Create DB
npm run migration:migrate
npm run migration:seed
```

### Testing
> run the tests
```
npm run test
``
