const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const HttpError = require('./app/lib/utils/http-error');

const app = express();

// Initialize db connection pool
require('./app/lib/utils/set-global-functions')();
require('./app/lib/db');

// USING ENV FILE
app.locals.ENV = process.env.NODE_ENV;
app.locals.ENV_DEVELOPMENT = process.env.NODE_ENV === 'local';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.get('/', (req, res) => res.status(200).send({
    message: 'Welcome.'
}));

// Route path
app.use('/api', require('./app/routes/indexRouter'));

// Test Case
app.get("/test", (request, response) => {
    response.status(500).send({ "message": "This is an error response" });
});

app.use(async (err, req, res, next) => {

    if (err.name === 'SequelizeValidationError') {
        res.status(422);
        return res.json(err.errors);
    } else if (err instanceof HttpError) {
        res.status(err.httpStatus);
        if (err.body) {
            return res.json(err.body);
        } else {
            return res.send({ message: err.message });
        }
    }
    console.log(err);
    return res.status(500).send({ message: 'Internal server error' });
});

module.exports = app;
