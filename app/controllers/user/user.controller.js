'use strict';
// const { models } = require(__base + '/app/lib/db');
const { models } = require('../../lib/db/');
const HttpError = require('../../lib/utils/http-error');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const saltRounds = 10;

async function generateHashedPassord(password) {
    const salt = await bcryptjs.genSalt(saltRounds);
    const hash = await bcryptjs.hash(password, salt);
    return hash;
}

let userController = {};

userController.registerUser = async (data) => {
    if (!data.email) throw new HttpError('Bad request', 'Email field is required', 400);
    if (!data.password) throw new HttpError('Bad request', 'Password field is required', 400);
    const dataPassword = await generateHashedPassord(data.password);
    const userData = {
        firstname: data.firstname,
        lastname: data.lastname,
        address: data.address,
        email: data.email,
        password: dataPassword
    }
    const resData = await models.user.create(userData);
    return resData;
}

userController.getRegisterUser = async () => {
    const data = await models.user.findAll();
    return data;
}

userController.userLogin = async (data) => {
    if (!data.email || !data.password) throw new HttpError('Bad request', 'Please enter Username or Password', 400);
    const resData = await models.user.find({
        where: { email: data.email }
    })
    // console.log(resData);
    if (!resData) throw new HttpError("Bad request", "Invalid credentials", 401);
    const matches = await bcryptjs.compare(data.password, resData.password);
    console.log(matches);
    if (!matches) throw new HttpError('Bad request', 'Invalid credentials', 401);

    let jwtSignObject = {
        id: resData.id,
        resData: {
            ...resData.toJSON()
        }
    };

    const token = jwt.sign(jwtSignObject, process.env.SECRET_KEY, { expiresIn: 604800 });

    const accessTokenData = {
        id: token,
        ttl: 604800,
        userId: resData.id
    }

    await models.AccessToken.create(accessTokenData);
    jwtSignObject.token = token;
    return jwtSignObject;

    //Destroy accessToken previous
    /*await models.AccessToken.destroy({
      where: { userId: user.id }
    })*/

}

userController.userLogout = async (tokenId) => {
    if (!tokenId) throw new HttpError('Bad request', 'Token is required', 400);
    const deletedAccessTokens = await models.AccessToken.destroy({ where: { id: tokenId } });
    if (deletedAccessTokens !== 1) { return Promise.reject(new HttpError('Bad Request', 'invalid token', 400)); }
    return { done: true };
}

module.exports = userController;