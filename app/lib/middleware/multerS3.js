"use strict";
const multer = require('multer');
const multerS3 = require('multer-s3');
const path = require('path');
const aws = require('aws-sdk');
const uuidv4 = require('uuid/v4');

const awsBucket = process.env.AWS_BUCKET;
const localDiskStoragePath = path.join(__dirname, '../../uploads');

const s3 = new aws.S3({
    'accessKeyId': process.env.AWS_ACCESS_KEY,
    'secretAccessKey': process.env.AWS_SECRET_KEY
});

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, localDiskStoragePath)
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

function uploadToAws(uploadFolder) {
    return multer({
        storage: multerS3({
            s3: s3,
            bucket: awsBucket + '/' + uploadFolder,
            contentType: multerS3.AUTO_CONTENT_TYPE,
            ACL: 'public-read',
            metadata: function (req, file, cb) {
                cb(null, { fieldName: file.fieldname });
            },
            key: function (req, file, cb) {
                cb(null, nameUploadInAWS(file.originalname));
            }
        })
    })
}


const uploadToLocal = multer({
    storage: storage
});

let nameUploadInAWS = function (filename) {
    return uuidv4() + path.parse(filename).ext;
};

module.exports = {
    uploadToLocal: uploadToLocal,
    uploadToAws: uploadToAws
}

