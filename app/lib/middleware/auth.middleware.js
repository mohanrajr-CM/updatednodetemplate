"use strict";
const jwtUtil = require('../utils/jwtUtils');
const HttpError = require('../utils/http-error');
const { models } = require('../models');

function authorizeUser() {
    return async function (req, res, next) {
        try {
            const jwt = req.headers.authorization || req.body.authorization || req.query.authorization;
            let verifiedJwt = await jwtUtil.verifyJwt(jwt);

            const foundToken = models.AccessToken.findById(jwt);
            if (!foundToken) throw new HttpError('Unauthorized', 'Invalid token', 401);


        } catch (err) {
            return next(new HttpError('Unauthorized', 'Invalid token', 401));
        }
    }
}

module.exports = {
    authorizeUser: authorizeUser
}


