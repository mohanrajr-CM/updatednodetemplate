'use strict';
const jwt = require('jsonwebtoken');
const HttpError = require('./http-error');

async function verifyJwt(inputJwt) {
  return jwt.verify(inputJwt, process.env.SECRET_KEY);
}

async function signJwt(inputObj, expiresIn = null) {
  try {
    if (!expiresIn) return await jwt.sign(inputObj, process.env.SECRET_KEY, {});
    return await jwt.sign(inputObj, process.env.SECRET_KEY, {expiresIn: expiresIn});
  } catch (err) {
    throw new HttpError('Bad service call', 'Cannot sign token', 500);
  }
}

module.exports = {
  verifyJwt: verifyJwt,
  signJwt: signJwt
};