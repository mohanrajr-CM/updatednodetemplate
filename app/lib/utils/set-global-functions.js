/* global __base */
module.exports = function () {
    if (global.GLOBAL_FUNCTIONS_DEFINED) return;
    console.log('setting global functions');
    global.GLOBAL_FUNCTIONS_DEFINED = 1;

    global.getController = function (name) {
        return require(__base + '/app/controllers/' + name + '/' + name + '.controller');
    };

    global.middleware = function (name) {
        return require(__base + '/app/lib/middleware/' + name + '.middleware');
    };
};