'use strict';
const bcryptjs = require('bcryptjs');
const saltRounds = 10;

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    address: DataTypes.STRING,
    email: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
      underscored: true,
      freezeTableName: true,
      tableName: 'user',
      hooks: {
        beforeCreate: beforeCreate,
        beforeBulkUpdate: beforeBulkUpdate
      }
    });

  user.associate = (models) => {
  };

  return user;
};

function beforeCreate(user, Options) {
  return new Promise((resolve, reject) => {
    user.email = user.email.toLowerCase();
    // user.active = 0;
    bcryptjs.genSalt(saltRounds, function (err, salt) {
      bcryptjs.hash(user.password, salt, (err, data) => {
        if (err) reject(err);
        user.password = data;
        resolve();
      });
    });
  });
}

function beforeBulkUpdate(user, Options) {
  if (!user.attributes.password) return;
  if (!user.attributes.email) return;
  user.email = user.email.toLowerCase();
  return new Promise((resolve, reject) => {
    bcryptjs.genSalt(saltRounds, function (err, salt) {
      bcryptjs.hash(user.attributes.password, salt, (err, data) => {
        if (err) reject(err);
        user.attributes.password = data;
        resolve();
      });
    });
  });
}