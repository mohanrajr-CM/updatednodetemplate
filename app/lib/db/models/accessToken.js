"use strict"

module.exports = (sequelize, DataTypes) => {
    const AccessToken = sequelize.define('AccessToken', {
        id: {
            type: DataTypes.TEXT,
            primaryKey: true,
            allowNull: false
        },
        ttl: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
            underscored: true,
            freezeTableName: true,
            tableName: 'accesstoken'
        });
    AccessToken.associate = () => {

    };
    return AccessToken;
};