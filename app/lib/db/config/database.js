require('../../../../configEnv');

module.exports = {
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || "r00t",
    database: process.env.DB_NAME || 'default_db',
    host: process.env.HOST || 'localhost',
    port: process.env.DB_PORT || 5432,
    dialect: process.env.DB_DIALECT || 'postgres',

    benchmark: process.env.CONFIG_SEQUELIZE_LOGGING === 'true',
    timezone: '+00:00',
    define: {
        instanceMethods: {
            touch: function () {
                return this.sequelize.query(`UPDATE "${this.$modelOptions.tableName}" SET "${this.$modelOptions.updatedAt}"='${moment().utcOffset(0).format('YYYY-MM-DD HH:mm:ss.SSS')}' WHERE id=${this.id}`);
            },

            toKeyString: function () {
                return `node_${this.$modelOptions.tableName}_${this.id}/${this.updated_at}`;
            }
        }
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
};
