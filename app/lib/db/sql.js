// const log = require('./../utils/log');
const Sequelize = require('sequelize');
const { types } = require('pg');
const dbConfig = require('./config/database');

const timestampOID = 1114;
types.setTypeParser(timestampOID, (stringValue) => {
    return new Date(Date.parse(stringValue + '+0000'));
});

const sqlConnection = new Sequelize(dbConfig);

sqlConnection.authenticate()
    .then(() => {
        // log.info(`Connection to database OK`);
        console.log(`Connection to database OK`);
    })
    .catch((err) => {
        //   log.warn(`Connection to database NOT OK`);
        //   log.error(err);
        console.log(`Connection to database NOT OK`);
    });

module.exports = sqlConnection;