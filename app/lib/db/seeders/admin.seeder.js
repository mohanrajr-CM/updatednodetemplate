'use strict';

const models = require('../models');
const bcryptjs = require('bcryptjs');
const saltRounds = 10;

async function generateHashedPassord(password) {
  const salt = await bcryptjs.genSalt(saltRounds);
  const hash = await bcryptjs.hash(password, salt);
  return hash;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const adminPassword = await generateHashedPassord('12345');
    const now = new Date();

    return queryInterface.bulkInsert('user', [
      {
        firstname: 'admin',
        lastname: 'admin',
        email: 'mohan.raj@codematrix.org',
        password: adminPassword,
        created_at: now,
        updated_at: now
      }
    ], {});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user', null, {});
  }
};