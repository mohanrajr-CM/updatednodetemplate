"use strict";
/* global getController */
/* global middleware */
const express = require('express');
const router = express.Router({ mergeParams: true });
// const userController = getController('user');
const userController = require('../../controllers/user/user.controller');

// Register User(POST):- /api/user/registerUser
router.post('/registerUser', (req, res, next) => {
    userController.registerUser(req.body)
        .then(result => res.status(200).json(result))
        .catch(next);
})

// Get User(GET):- /api/user/getUser
router.get('/getUser', (req, res, next) => {
    userController.getRegisterUser()
        .then(result => res.status(200).json(result))
        .catch(e => {
            console.log(e);
        });
})

// Login(POST):- /api/user/login
router.post('/login', (req, res, next) => {
    userController.userLogin(req.body)
        .then(result => res.status(200).json(result))
        .catch(next);
})

// Logout(POST):- /api/user/logout
router.post('/logout', (req, res, next) => {
    userController.userLogout(req.headers.authorization)
        .then(result => res.status(200).json(result))
        .catch(next);
})

module.exports = router;